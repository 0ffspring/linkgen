#!/bin/bash

LINK_HOLDER_PATH="linkholder"
FILES_HOLDER_PATH="files"
RESULT_HOLDER_PATH="result"
source $LINK_HOLDER_PATH
BASE_LINK=$LINK
echo "Base link is: $LINK"

if [ -f $RESULT_HOLDER_PATH ];
then
   echo "File with results already exists. Do you wish to clear it first (Type 1 or 2)?"
    select yn in "Yes" "No"; do
	case $yn in
	    Yes ) > $RESULT_HOLDER_PATH; break;;
	    No ) break;;
	esac
    done
fi

echo "Start iterating file names..."
while IFS='' read -r line || [[ -n "$line" ]]; do
    echo "$BASE_LINK$line" >> $RESULT_HOLDER_PATH
    echo "Created link: $BASE_LINK$line"
done < "$FILES_HOLDER_PATH"